---
name: 问题反馈
about: 不建议无 Linux、基础网络知识经验者自行建立服务器。
title: ''
labels: help wanted
assignees: yiguihai

---

遇到问题请先阅读wiki文档和搜索资料再提问，提问时，描述清楚网络环境，并给出错信息截图或提示文字。
